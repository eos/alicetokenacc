#include "TSealedEnvelope.hh"
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

int main() {
  struct timeval abs_start_time;
  struct timeval abs_start1_time;
  struct timeval abs_start2_time;
  struct timeval abs_start3_time;
  struct timezone tz;
  
  gettimeofday (&abs_start_time, &tz);

  int32_t cnt=0;
  TSealedEnvelope* sealed = new TSealedEnvelope("key.pem","pkey.pem","key.pem","pkey.pem","Blowfish","Andreas.Joachim.Peters",0);
  TSealedEnvelope* decoder = GetEnvelope("key.pem","pkey.pem","Blowfish","Andreas.Joachim.Peters",0,0);
  if (!sealed->Initialize(TSE_CODEC)) {
    exit(-1);
  }

  if (!decoder->Initialize(TSE_DECODE)) {
    exit(-1);
  }

  decoder->UnLock();
  //  decoder->Verbose(1);
  //  sealed->Verbose(1);
  float encodingtime=0;
  float decodingtime=0;


  while (1) {   
    gettimeofday (&abs_start1_time, &tz);
    cnt++;
    std::string env = sealed->encodeEnvelope(std::string("Das ist ein neuer Test"),0,std::string("none"));
    //    std::cout  << env;
    gettimeofday (&abs_start2_time, &tz);
    decoder = GetEnvelope("key.pem","pkey.pem","Blowfish","Andreas.Joachim.Peters",0,0);
    std::string unenv = decoder->decodeEnvelope(env);
    decoder->UnLock();
    
    gettimeofday (&abs_start3_time, &tz);

    float abs_time1=((float)((abs_start2_time.tv_sec - abs_start1_time.tv_sec) *1000000 +
			    (abs_start2_time.tv_usec - abs_start1_time.tv_usec)))/1000.0;
    float abs_time2=((float)((abs_start3_time.tv_sec - abs_start2_time.tv_sec) *1000000 +
			    (abs_start3_time.tv_usec - abs_start2_time.tv_usec)))/1000.0;

    float totaltime=((float)((abs_start3_time.tv_sec - abs_start_time.tv_sec) *1000000 +
			    (abs_start3_time.tv_usec - abs_start_time.tv_usec)))/1000.0;

    encodingtime+=abs_time1;
    decodingtime+=abs_time2;

    //    std::cout << unenv;
    if (!(cnt%500)) {
      decoder->PrintHeader();
      printf("Performance: Codings: %.02f encodings/s\n",1000*cnt/encodingtime);
      printf("Performance: Codings: %.02f decodings/s\n",1000*cnt/decodingtime);
      printf("Performance: Overall  %.02f en-decodings/s\n",1000*cnt/totaltime);

    }
    if (cnt==1000)
      exit(0);
    if (!(cnt%10000)) {
      usleep(5000000);
    }
    sealed->Reset();
    decoder->Reset();
  }
}
