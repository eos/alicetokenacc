//         $Id: XrdAliceTokenAcc.cc,v 1.13 2007/10/04 01:34:19 abh Exp $

#include "XrdOuc/XrdOucTrace.hh"
#include "XrdOuc/XrdOucEnv.hh"
#include "XrdSys/XrdSysError.hh"
#include "XrdSec/XrdSecEntity.hh"
#include "XrdOuc/XrdOucString.hh"
#include "XrdOuc/XrdOucStream.hh"

#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/hmac.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>

#include <openssl/evp.h>
#include <openssl/objects.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/rsa.h>
#include <openssl/engine.h>
#include <openssl/sha.h>

#include "TTokenAuthz.hh"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <fstream>
#include <netinet/in.h>

#include "XrdVersion.hh"

XrdVERSIONINFO(XrdAccAuthorizeObject,"AliceTokenAcc");

XrdSysError TkEroute(0,"AliceTokenAcc");
XrdOucTrace TkTrace(&TkEroute);

#include "XrdAliceTokenAcc.hh"

XrdOucHash<XrdOucString>*  XrdAliceTokenAcc::NoAuthorizationHosts;
XrdOucTList*               XrdAliceTokenAcc::NoAuthorizationHostWildcards;
XrdOucString               XrdAliceTokenAcc::TruncatePrefix="";
XrdSysMutex*               XrdAliceTokenAcc::CryptoMutexPool[128];
EVP_PKEY*                  XrdAliceTokenAcc::EVP_RemotePublicKey;
#define IS_SLASH(s) (s == '/')


/******************************************************************************/
/*             T h r e a d - S a f e n e s s   F u n c t i o n s              */
/******************************************************************************/
static unsigned long aliceauthzssl_id_callback(void) {
  return (unsigned long)XrdSysThread::ID();
}

void aliceauthzssl_lock(int mode, int n, const char *file, int line)
{
  if (mode & CRYPTO_LOCK) {
    if (XrdAliceTokenAcc::CryptoMutexPool[n]) {
      XrdAliceTokenAcc::CryptoMutexPool[n]->Lock();
    }
  } else {
    if (XrdAliceTokenAcc::CryptoMutexPool[n]) {
      XrdAliceTokenAcc::CryptoMutexPool[n]->UnLock();
    }
  }
}

bool
XrdAliceTokenAcc::MatchWildcard(const char* host) {
  XrdOucTList* lp=NoAuthorizationHostWildcards;

  while (lp) {
    XrdOucString match = host;
    XrdOucString pattern = lp->text;
    
    // check for ? matches
    if (pattern.find('?')!= STR_NPOS) {
      int pos=0;
      while ( (pos = pattern.find('?',pos)) != STR_NPOS) {
	if (pos <= match.length()) {
	  match.erase(pos,pos+1);
	  match.insert('?',pos);
	}
      }
      if (match == host) {
	return true;
      }
      lp = lp->next;
      continue;
    }

    if (pattern.find('*')!=STR_NPOS) {
      XrdOucString startswith;
      XrdOucString stopswith;
      startswith.assign(pattern,0, pattern.find('*')-1);
      stopswith.assign(pattern, pattern.find('*')+1);
      if (debug) { 
	TkTrace.Beg("Match");
	cerr <<"Match by '*': Startswith " << startswith.c_str() << " Stopswith " <<stopswith.c_str();
	TkTrace.End();
      }
      if ( match.beginswith(startswith) && match.endswith(stopswith) ) {
	return true;
      }
    }

    int n1,n2,n3;
    n1=n2=n3=0;
    if ( ((n1=pattern.find('[')) !=STR_NPOS) && ((n2=pattern.find(']')) != STR_NPOS) ) {
      int a,b,c;
      a=b=c=0;
      XrdOucString sa,sb,sc; 
      if (debug) { 
	TkTrace.Beg("Match");
	cerr <<"Match by '[a-b]': n1 " << n1 << " n2 " << n2;
	TkTrace.End();
      }
      if (n1 < n2) {
	n3 = pattern.find('-',n1+1);
	
	if ( (n3>n1) && (n3 < n2) ) {
	  sa.assign(pattern,n1+1,n3-1);
	  sb.assign(pattern,n3+1,n2-1);
	  a = atoi(sa.c_str());
	  b = atoi(sb.c_str());

	  XrdOucString startswith;
	  XrdOucString stopswith;
	  startswith.assign(pattern,0, pattern.find('[')-1);
	  stopswith.assign(pattern, pattern.find(']')+1);

	  if (debug) { 
	    TkTrace.Beg("Match");
	    cerr <<"Match by '[a-b]': Startswith " << startswith.c_str() << " Stopswith " <<stopswith.c_str();
	    TkTrace.End();
	  }
	  if ( match.beginswith(startswith) && match.endswith(stopswith) ) {
	    // see the number in the host
	    if ( (n3-1) < match.length()) {
	      sc.assign(match,n1,n3-2);
	      ErrnoMutex.Lock();
	      errno = 0;
	      c = (int) strtol(sc.c_str(),NULL,0);
	      if (debug) { 
		TkTrace.Beg("Match");
		cerr <<"Match by '[a-b]': Converted " << sc.c_str() << " to " << c;
		TkTrace.End();
	      }


	      if (errno != 0) {
		ErrnoMutex.UnLock();
		lp = lp->next;
		continue;
	      }
	      ErrnoMutex.UnLock();
	      if ( (a <= c) && (c <= b) ) {
		return true;
	      }
	    }
	  }
	}
      }
    }
    lp = lp->next;
  }
  return false;
}


XrdAccPrivs 
XrdAliceTokenAcc::Access(const XrdSecEntity    *client,
			 const char            *path,
			 const Access_Operation oper,
			 XrdOucEnv             *Opaque) 
{
  if (multiprocess) {
    // send the authz information over ZMQ
    std::string authz;
    const char* sauthz=0;
    if ( sauthz = Opaque->Get("authz") ) {
      if (getenv("ALICETOKENDEBUG")) {
	fprintf(stderr,"# XrdAliceTokenAcc::Access multiprocess access\n");
      }
      authz = sauthz;
    } else {
      if (getenv("ALICETOKENDEBUG")) {
	fprintf(stderr,"# XrdAliceTokenAcc::Access no token bypass\n");
      }
      // no token, let's just go directly to the library
      return AccessExec(client, path, oper, Opaque);
    }
    if (getenv("ALICETOKENDEBUG")) {
      fprintf(stderr,"# XrdAliceTokenAcc::Access encode access authz='%s'\n", authz.c_str());
    }
    std::string access = EncodeAccess(client, path, oper, authz.c_str());
    if (getenv("ALICETOKENDEBUG")) {
      fprintf(stderr,"# XrdAliceTokenAcc::Access send authz\n");
    }
    int rc = zmq->Send(access);
    if (rc < 0) {
      return XrdAccPriv_None;
    } else {
      return (XrdAccPrivs)rc;
    }
  } else {
    // call the Access function directly
    return AccessExec(client, path, oper, Opaque);
  }
}

XrdAccPrivs 
XrdAliceTokenAcc::AccessExec(const XrdSecEntity    *client,
			     const char            *path,
			     const Access_Operation oper,
			     XrdOucEnv             *Opaque) 
{
  TAuthzXMLreader* authz=0;
  int envlen ;
  
  XrdOucString UnprefixedPath="";
  XrdOucString sopaque = "";

  const char* opaque = "";

  if (Opaque) {
    // we have to remove all double quotes - it is convenient to allow double quotes to pass opaque information via shell commands
    sopaque = Opaque->Env(envlen);
    while (sopaque.replace("\"","")) {}
    opaque = sopaque.c_str();
  }

  std::map<std::string,std::string> env;

  TTokenAuthz::Tokenize(opaque,env,"&");


  XrdOucString protocol = client->prot;

  // trusted machines can do anything!
  if (protocol == "sss") {
    return XrdAccPriv_All;
  }
  if (protocol == "krb5") {
    return XrdAccPriv_All;
  }
  if (protocol == "gsi") {
    return XrdAccPriv_All;
  }

  if (1) {
    // see if we have to erase some prefix
    if (TruncatePrefix.length()) {
      UnprefixedPath=path;
      UnprefixedPath.replace(TruncatePrefix.c_str(),"");
      if (!UnprefixedPath.beginswith('/')) {
	UnprefixedPath.insert('/',0);
      }
      path = UnprefixedPath.c_str();
    }
    
    std::string vo="*";
    
    // if we have the vo defined in the credentials, use this one
    if (client) {
      if ((client->vorg) && (strlen(client->vorg)))
	vo = client->vorg;
    }
    
    // set the certificate, if we have one
    const char* certsubject=0;
    if (client) {
      if ((client->name) && (strlen(client->name))) {
	certsubject = client->name;
      }
    }
    
    TTokenAuthz* tkauthz = 0;
    debug = getenv("ALICETOKENDEBUG")?true:false;
    
    if (debug) {
      tkauthz = TTokenAuthz::GetTokenAuthz("xrootd",true); // with debug output
    } else {
      tkauthz = TTokenAuthz::GetTokenAuthz("xrootd",false);// no debug output
    }
    
    
    // set the opening mode
    std::string authzopenmode="";
    
    switch(oper) {
    case AOP_Create :
    case AOP_Update:
      authzopenmode="write-once";
      break;
    case AOP_Delete :
      break;
    case AOP_Read :
      authzopenmode="read";
      break;
    case AOP_Stat :
      return XrdAccPriv_Lookup;
    case AOP_Readdir :
      return XrdAccPriv_Readdir;
    default:
      return XrdAccPriv_None;
    }
    
    // allow the quick route ... e.g. check if we can grant an operation without decoding the envelope
    
    // check if the directory asked is exported
    if (tkauthz->PathIsExported(path,vo.c_str(),certsubject)) {
      // check the host
      if (client) {
	if (client->host) {
	  // if this is a authorization free host, allow it!
	  if (NoAuthorizationHosts->Find(client->host)) {
	    return XrdAccPriv_All;
	  }
	  if (MatchWildcard(client->host)) 
	    return XrdAccPriv_All;
	}
      }   
      if (!tkauthz->PathHasAuthz(path,authzopenmode.c_str(),vo.c_str(),certsubject)) {
	// the pass through
	return XrdAccPriv_All;
      } else {
	if ( ((env["authz"].length()) == 0 ) || (env["authz"] == "alien") || (env["authz"] == "alien?")) {
	  // path needs authorization
	  TkEroute.Emsg("Access",EACCES,"give access for lfn - path has to be authorized",path);
	  return XrdAccPriv_None;
	}
      }
    } else {
      // path is not exported for this VO
      TkEroute.Emsg("Access",EACCES,"give access for lfn - path not exported",path);
      return XrdAccPriv_None;
    }
    
    int garesult=0;
    float t1,t2;
    
    garesult = tkauthz->GetAuthz(path,opaque,&authz,debug,&t1,&t2);

    // mantain ALICE symlinks
    if ( getenv("ALICE_TOKEN_SYMLINKS") ) {
      char localpath[16384];
      //      XrdOfsOss->GenLocalPath(path, localpath);
      XrdOucString linkname = getenv("ALICE_TOKEN_SYMLINKS");
      XrdOucString linktarget=localpath;
      XrdOucString linkpointer = "";
      linkname += "/"; linkname += authz->GetKey(path,"lfn");
      struct stat buf;
      if (!stat(linkname.c_str(), &buf)) {
        char linkdestination[4096];
        if ( (readlink (linkname.c_str(), linkdestination, sizeof(linkdestination))) > 0) {
          linkpointer = linkdestination;
        }
      }

      if (linktarget != linkpointer) {
        int rc = unlink(linkname.c_str());
        if (rc) rc = 0;
        // link does not point properly
        int pos=0;
        int retc=0;
        XrdOucString newpath = linkname.c_str();
        while(newpath.replace("//","/")) {};
        int rpos=STR_NPOS;
        while ((rpos = newpath.rfind("/",rpos))!=STR_NPOS) {
          XrdOucString existspath;
          existspath.assign(newpath,0,rpos);

          if (!stat(existspath.c_str(), &buf)) {
            // this exists, now creat until the end
            int fpos= rpos+2;
            while ( (fpos = newpath.find("/",fpos)) != STR_NPOS ) {
              XrdOucString createpath;
              createpath.assign(newpath,0,fpos);
              mkdir(createpath.c_str(),S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
              fpos++;
            }
            break;
          }
          rpos--;
        }
        // create a link
        retc = symlink(linktarget.c_str(),linkname.c_str());
        if (retc) retc=0;
      }
    }

    TkTrace.Beg("Access");
    cerr <<"[ " << getpid() << " ] Time for Authz decoding: " << t1 << " ms" << " / " << t2 << "ms =>" << path ;
    TkTrace.End();
    
    if (garesult != TTokenAuthz::kAuthzOK) {
      // if the authorization decoding failed for any reason
      TkEroute.Emsg("Access",tkauthz->PosixError(garesult),tkauthz->ErrorMsg(garesult) , path);
      if (authz) delete authz;
      return XrdAccPriv_None;
    }
    
    // check the access permissions
    if (oper == AOP_Read) {
      // check that we have the READ access
      if (strcmp(authz->GetKey((char*)path,"access"), "read")) {
	// we have no read access
	TkEroute.Emsg("Access",EACCES,"have read access for lfn" , path);
	if (authz) delete authz;
	return XrdAccPriv_None;
      }
    } else {
      if ( (oper == AOP_Create) || (oper == AOP_Stat) || (oper == AOP_Update) ) {
	// check that we have the WRITE access
	if (strcmp(authz->GetKey((char*)path,"access"), "write-once")) {
	  // we have no write-once access
	  TkEroute.Emsg("Access",EACCES,"have write access for lfn" , path);
	  if (authz) delete authz;
	  return XrdAccPriv_None;
	}
      } else {
	if ( (oper == AOP_Delete) ) {
	  // check that we have the READ-WRITE access
	  if (strcmp(authz->GetKey((char*)path,"access"), "delete")) {
	    // we have no deletion access
	    TkEroute.Emsg("Access", EACCES, "have delete access for lfn", path);
	    if (authz) delete authz;
	    return XrdAccPriv_None;
	  }
	} else {
	  TkEroute.Emsg("Access", EACCES, "have access for lfn", path);
	  if (authz) delete authz;
	  return XrdAccPriv_None;
	}
      }  
    }
    
    // get the turl
    const char* newfilename = TTokenAuthz::GetPath(authz->GetKey((char*)path,"turl"));
    std::string copyfilename = newfilename;
    
    // check if the asked filename is exported
    if (!tkauthz->PathIsExported(newfilename,vo.c_str())) {
      // path is not exported for this VO
      TkEroute.Emsg("Acess", EACCES, "give access for turl - path not exported", newfilename);
      if (authz) delete authz;
      return XrdAccPriv_None;
    }
    
    // do certifcate check, if it is required
    if (tkauthz->CertNeedsMatch(newfilename,vo.c_str())) {
      if (certsubject != authz->GetKey((char*)path,"certsubject")) {
	TkEroute.Emsg("Access", EACCES, "give access for turl - certificate subject does not match", newfilename);
	return XrdAccPriv_None;
      }
    }
    
    if (authz)delete authz;

    TkTrace.Beg("Access");
    cerr <<"[ " << getpid() << " ] access granted for path " << path ;
    TkTrace.End();
    return XrdAccPriv_All;
  }
}

bool
XrdAliceTokenAcc::Configure(const char* ConfigFN) {
  char *var;
  const char *val;
  int  cfgFD, retc, NoGo = 0;

  NoAuthorizationHosts = new XrdOucHash<XrdOucString>();
  NoAuthorizationHostWildcards = NULL;
  TruncatePrefix = "";

  XrdOucStream Config(&TkEroute, getenv("XRDINSTANCE"));
  if( (!ConfigFN) || (!(*ConfigFN))) {
    TkEroute.Emsg("Config", "Configuration file not specified.");
    return false;
  } else {
    // Try to open the configuration file.
    //
    if ( (cfgFD = open(ConfigFN, O_RDONLY, 0)) < 0)
      return TkEroute.Emsg("Config", errno, "open config file", ConfigFN);
    Config.Attach(cfgFD);
    // Now start reading records until eof.
    //
    
    while((var = Config.GetMyFirstWord())) {
      if (!strncmp(var, "alicetokenacc.", 14)) {
        var += 14;
	if (!strcmp("noauthzhost",var)) {
          val = Config.GetWord();
	  
          TkEroute.Say("=====> alicetokenacc.noauthzhost: ", val,"");
	  if ((strchr(val,'*')) || (strchr(val,'[') && strchr(val,']')) || (strchr(val,'?'))) {
	    // add a wildcard pattern to the list
	    NoAuthorizationHostWildcards = new XrdOucTList(val, 0, NoAuthorizationHostWildcards);
	  } else {
	    NoAuthorizationHosts->Add(val,new XrdOucString(val));
	  }
        }
	if (!strcmp("truncateprefix",var)) {
	  val = Config.GetWord();
	  
	  TkEroute.Say("=====> alicetokenacc.truncateprefix: ", val,"");
	  TruncatePrefix=val;
	}
	if (!strcmp("multiprocess", var)) {
	  val = Config.GetWord();
	  TkEroute.Say("=====> alicetokenacc.multiprocess: ", val,"");
	  multiprocess = atoi(val);
	  if (multiprocess < 0 ) {
	    multiprocess=0; 
	    TkEroute.Say("=====> alicetokenacc.multiprocess: negativ setting - disabling", val,"");
	  }
	  if (multiprocess > 128) {
	    multiprocess = 128;
	    TkEroute.Say("=====> alicetokenacc.multiprocess: limited to max 128" "","");
	  }
	}
      }
    }
  }
  
  // create the crypto mutex pool

  for (size_t i=0; i< 128; i++) {
    XrdAliceTokenAcc::CryptoMutexPool[i] = new XrdSysMutex();
  }

  // set callback functions
  CRYPTO_set_locking_callback(aliceauthzssl_lock);
  CRYPTO_set_id_callback(aliceauthzssl_id_callback);
  return true;
}

bool
XrdAliceTokenAcc::Init() {
  std::list<std::string> configpaths;
  
  // find the location of the keys to sign response envelopes
  if (getenv("TTOKENAUTHZ_AUTHORIZATIONFILE")) {
    configpaths.push_back(std::string(getenv("TTOKENAUTHZ_AUTHORIZATIONFILE")));
  } else {
    fprintf(stderr,"=====> XrdAliceTokenAcc: No Authorizationfile set via environment variable 'TTOKENAUTHZ_AUTHORIZATIONFILE'\n");
  }
  
  std::string extraname = "xrootd/";
  
  configpaths.push_back("/etc/grid-security/" + extraname +"TkAuthz.Authorization");
  if (getenv("HOME")) {
    std::string pstring = getenv("HOME");
    pstring +=  "/.globus/";
    pstring += extraname;
    pstring += "TkAuthz.Authorization";
    configpaths.push_back(pstring);
    pstring = getenv("HOME");
    pstring +=  "/.authz/";
    pstring += extraname;
    pstring += "TkAuthz.Authorization";
    configpaths.push_back(pstring);
  }
  
  std::list<std::string>::iterator confname;
  
  std::string authorizationfile="";
  
  for (confname=configpaths.begin(); confname != configpaths.end(); ++confname) {
    struct stat buf;
    if (!::stat((*confname).c_str(),&buf)) {
      if ( (buf.st_mode & S_IWGRP) || (buf.st_mode & S_IWGRP) ) {
	fprintf(stderr,"=====> XrdAliceTokenAcc: Authorizationfile '%s' has insecure permission! Not used!\n",(*confname).c_str());
      } else {
	fprintf(stderr,"=====> XrdAliceTokenAcc: Using Authorizationfile '%s'!\n",(*confname).c_str());
	authorizationfile=(*confname);
	break;
      }
    } else {
      fprintf(stderr,"=====> XrdAliceTokenAcc: No Authorizationfile like '%s' found\n",(*confname).c_str());
    }
  }
  
  EVP_RemotePublicKey = 0;

  if (authorizationfile.length()) {
    // see if we can read this file
    int fd = ::open(authorizationfile.c_str(),0,0);
    if (fd <1) {
      fprintf(stderr,"=====> XrdAliceTokenAcc: Unable to read authorization file '%s'\n", authorizationfile.c_str());
      return false;
    }
    close(fd);
  }

  if (authorizationfile.length()) {
    char buffer[1025];
    std::ifstream authzfile(authorizationfile.c_str());
    
    while (authzfile.getline(buffer,sizeof(buffer))) {
      int length=strlen(buffer);
      // ignore comments
      if (buffer[0] == '#')
	continue;
      if (length == 0)
	continue;

      XrdOucString pubkey = buffer;

      pubkey.erase(length);

      size_t pos;
      if ( (pos = pubkey.find("PUBKEY:")) != STR_NPOS ) {
	pubkey.erase(0,pos+7);
      } else {
	continue;
      }
      
      fprintf(stderr, "=====> XrdAliceTokenAcc: Public key in use is %s\n", pubkey.c_str());
      EVP_RemotePublicKey  = ReadPublicKey(pubkey.c_str());
      if (!EVP_RemotePublicKey) {
	fprintf(stderr, "=====> XrdAliceTokenAcc: Cannot load public key !\n");
	return false;
      }
    }
  } else {
    fprintf(stderr, "=====> XrdAliceTokenAcc: no valid configuration file - will not verify response envelopes\n");
    return false;
  }
  
  debug=false;

  if (MultiProcess()) {
    zmq = new XrdZMQ((*this), multiprocess);
    if (!zmq->RunServer()) {
      fprintf(stderr,"=====> XrdAliceTokenAcc: multiprocess server startup failed\n");
      return false;
    }
    if (!zmq->SetupClients()) {
      fprintf(stderr,"=====> XrdAliceTokenAcc; multiprocess clien setup failed\n");
      return false;
    }
  }

  return true;
}

XrdAliceTokenAcc::~XrdAliceTokenAcc() {
  if (zmq) {
    delete zmq;
  }
}
/* XrdAccAuthorizeObject() is called to obtain an instance of the auth object
   that will be used for all subsequent authorization decisions. If it returns
   a null pointer; initialization fails and the program exits. The args are:

   lp    -> XrdSysLogger to be tied to an XrdSysError object for messages
   cfn   -> The name of the configuration file
   parm  -> Parameters specified on the authlib directive. If none it is zero.
*/

extern "C" XrdAccAuthorize *XrdAccAuthorizeObject(XrdSysLogger *lp,
                                              const char   *cfn,
                                              const char   *parm) 
{
  TkEroute.SetPrefix("XrdAliceTokenAcc::");
  TkEroute.logger(lp);
  TkEroute.Say("++++++ (c) 2008 CERN/IT-DM-SMD ",
	       "AliceTokenAcc (Alice Token Access Authorization) v 1.0");
  XrdAccAuthorize* acc = (XrdAccAuthorize*) new XrdAliceTokenAcc();
  if (!acc) {
     TkEroute.Say("------ AliceTokenAcc Allocation Failed!");
     std::abort();
     return 0;
  }

  if (!((XrdAliceTokenAcc*)acc)->Configure(cfn) || (!((XrdAliceTokenAcc*)acc)->Init())) {
    TkEroute.Say("------ AliceTokenAcc Initialization Failed!");
    delete acc;
    std::abort();
    return 0;
  } else {
    TkEroute.Say("------ AliceTokenAcc initialization completed");
    return acc;
  }
}






EVP_PKEY*
XrdAliceTokenAcc::ReadPublicKey(const char* certfile) {
  FILE *fp = fopen (certfile, "r");
  X509 *x509;
  EVP_PKEY *pkey;

  if (!fp) {
     return NULL;
  }

  x509 = PEM_read_X509(fp, NULL, 0, NULL);

  if (x509 == NULL)
  {
     ERR_print_errors_fp (stderr);
     return NULL;
  }

  fclose (fp);

  pkey=X509_extract_key(x509);

  X509_free(x509);

  if (pkey == NULL)
     ERR_print_errors_fp (stderr);

  return pkey;

}


char *XrdAliceTokenAcc::unbase64(unsigned char *input, int length) {
    BIO *b64, *bmem;
    
    char *buffer = (char *)malloc(length);
    memset(buffer, 0, length);
        
    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bmem = BIO_new_mem_buf(input, length);
    bmem = BIO_push(b64, bmem);
    
    BIO_read(bmem, buffer, length);
    
    BIO_free_all(bmem);
    
    return buffer;
}

// Serializer                                                                                                                  
std::string 
XrdAliceTokenAcc::EncodeAccess(const XrdSecEntity* entity,
			       const char * path,
			       const Access_Operation oper,
			       const char* authz)
{
  std::string access;
  access += entity->prot;
  access += "|";
  if (entity->host) {
    access += entity->host;
  }
  access += "|";
  if (entity->name) {
    access += entity->name;
  }
  access += "|";
  if (entity->vorg) {
    access += entity->vorg;
  }
  access += "|";
  access += std::to_string(oper);
  access += "|";
   XrdOucString sescape=path;
  while ( sescape.replace("_#PIPE#_","___@#$%___") ) {}
  while ( sescape.replace("|","_#PIPE#_")) {}
  access += sescape.c_str();
  access += "|";
  if (authz) {
    access += authz;
  }
  access += "|";
  if (getenv("ALICETOKENDEBUG")) {
    fprintf(stderr,"# XrdAliceTokenAcc::EncodeAccess '%s'\n", access.c_str());
  }
  return access;
}


// Tokenize a string
void
XrdAliceTokenAcc::Tokenize(const std::string& str,
			   std::vector<std::string>& tokens,
			   const std::string& delimiters)
{
  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  std::string::size_type pos = str.find_first_of(delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos) {
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_of(delimiters, pos);

    if (lastPos != std::string::npos) {
      lastPos++;
    }

    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }
}


// Deserializer                                                                                                                
bool 
XrdAliceTokenAcc::DecodeAccess(
			       const std::string& encoded,
			       XrdSecEntity& entity,
			       std::string& path,
			       Access_Operation& oper,
			       std::string& authz)
{
  std::vector<std::string>token;
  Tokenize(encoded, token, "|");
  strncpy(entity.prot,token[0].c_str(), XrdSecPROTOIDSIZE);
  if (token.size() != 8) {
    return false;
  }

  if (getenv("ALICETOKENDEBUG")) {
    for (int i =0 ; i< 7; ++i) {
      fprintf(stderr,"# [%d] : %s\n", i, token[i].c_str());
    }
  }
  if (token[1].length()) { entity.host = strdup(token[1].c_str()); }
  if (token[2].length()) { entity.name = strdup(token[2].c_str()); } 
  if (token[3].length()) { entity.vorg = strdup(token[3].c_str()); }
  if (token[4].length()) { oper = (Access_Operation)(std::strtol(token[4].c_str(), 0,10));} 
  XrdOucString sescape=token[5].c_str();
  while (sescape.replace("_#PIPE#_","|")) {}
  while (sescape.replace("___@#$%___","_#PIPE#_")) {}
  path = sescape.c_str();
  authz = token[6];
  return true;
}

